


##########################
# ENV Tags
##########################
/*
variable "prefix" {
    type        = string
  default = "firm-1"
}

variable "environment" {
    type        = string
    
}



locals {
  prefix = join("-", [var.prefix, var.environment])
  common_tags = {
    ManagedBy = "Terraform"
  }
}
*/


##########################
# S3 backend declaration
##########################

variable "s3_bucket_name" {
  type        = string
  description = "S3 bucket name defined"
}

variable "s3_key" {
  type        = string
  description = "S3 key"
}

variable "s3_region" {
  type        = string
  default     = "us-east-1"
  description = "S3 region"
}
