
resource "aws_vpc" "main_vpc" {
  cidr_block = "10.0.0.0/16"
  
  tags = {
      Name = "web-vpc"
  }
}

resource "aws_internet_gateway" "main_gw" {
  vpc_id = aws_vpc.main_vpc.id

  tags = {
    Name = "main_gw"
  }
}

######################
#Public Subnet
######################
resource "aws_subnet" "main_public_subnet" {
  vpc_id     = aws_vpc.main_vpc.id
  cidr_block = "10.0.1.0/24"
  availability_zone = ""
  map_public_ip_on_launch = true

  tags = {
    Name = "Main_public_subnet"
  }
}

resource "aws_route_table" "main_route_table" {
  vpc_id = aws_vpc.main_vpc.id

}


resource "aws_route_table_association" "main_route_table" {
  subnet_id      = aws_subnet.main_public_subnet.id
  route_table_id = aws_route_table.main_route_table.id

}

resource "aws_route" "public_internet_access" {
  route_table_id         = aws_route_table.main_route_table.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.main_gw.id
}

resource "aws_eip" "public_elastic_ip" {
  vpc = true

  tags = {
    name = "elastic_eip"
  }
}

resource "aws_nat_gateway" "public_nat_gw" {
  allocation_id = aws_eip.public_elastic_ip.id
  subnet_id     = aws_subnet.main_public_subnet.id

  tags = {
    name = "public_nat_gateway"
  }
}


###################
# Private subnet
###################

resource "aws_subnet" "main_private_subnet" {
  vpc_id     = aws_vpc.main_vpc.id
  cidr_block = "10.0.2.0/24"
  availability_zone = ""
  

  tags = {
    Name = "Main_private_subnet"
  }
}

resource "aws_route_table_association" "main_route_table" {
  subnet_id      = aws_subnet.main_private_subnet.id
  route_table_id = aws_route_table.main_route_table.id

}

resource "aws_route" "public_internet_access" {
  route_table_id         = aws_route_table.main_route_table.id
  nat_gateway_id          = aws_nat_gateway.public_nat_gw.id
  destination_cidr_block = "0.0.0.0/0"
  
}



