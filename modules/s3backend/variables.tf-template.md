variable "s3_bucket_name" {
  type        = string
  description = "S3 bucket name defined"
}

variable "s3_key" {
  type        = string
  description = "S3 key"
}

variable "s3_region" {
  type        = string
  default     = "us-east-1"
  description = "S3 region"
}
